#!/bin/bash

# Download the software
curl -L -O https://releases.hashicorp.com/vagrant/1.7.2/vagrant_1.7.2.dmg
curl -L -O http://download.virtualbox.org/virtualbox/5.0.12/VirtualBox-5.0.12-104815-OSX.dmg

# Vagrant: Install and clean up 
sudo hdiutil attach vagrant_1.7.2.dmg
sudo installer -package /Volumes/Vagrant/Vagrant.pkg -target /
sudo hdiutil detach /Volumes/Vagrant
rm vagrant_1.7.2.dmg

# VirtualBox: Install and clean up
sudo hdiutil attach VirtualBox-5.0.12-104815-OSX.dmg
sudo installer -package /Volumes/VirtualBox/VirtualBox.pkg -target /
sudo hdiutil detach /Volumes/VirtualBox
rm VirtualBox-5.0.12-104815-OSX.dmg
